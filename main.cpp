#include "MyFreenectDevice.h"
#include <highgui.h>
#include <vector>
#include <cmath>
#include "BlobResult.h"
#include "blob.h"
#include <ctime>

#include <iostream>
#include <fstream>

#include "assert.h"

#define DEBUG 0

using namespace cv; 
using namespace std;

Point lastKnown;
const double maxBlobArea=2000;
const double minBlobArea=0;

const int imgWidth = 640;
const int imgHeight = 480;

ofstream logFile;
clock_t currentTime;

void onMouse( int event, int x, int y, int, void* ) {
    // only handle left click for now
    if (event != EVENT_LBUTTONDOWN) {
        return;
    }
    lastKnown.x = x;
    lastKnown.y = y;
}

#define DEPTH_X_RES 640
#define DEPTH_Y_RES 480
//http://openkinect.org/wiki/Imaging_Information#Depth_Camera
// we may still need to do the checkerboard
void my_camera_to_world(int cx, int cy, int cz, double* wx, double* wy, double *wz)
{
#if USE_RAW_DEPTH
    double z_meter = 0.1236 * tan((double)cz / 2842.5 + 1.1863) - 0.037;
#else
    double z_meter = cz;
#endif
    const double minDistance = -10;
    const double scaleFactor = 0.00315;
    if (wx)
        *wx = (cx - DEPTH_X_RES/2)*(z_meter+minDistance)*scaleFactor;
    if (wy)
        *wy = (cy - DEPTH_Y_RES/2)*(z_meter+minDistance)*scaleFactor;
    if (wz)
        *wz = z_meter;
}

static bool doTrack = false;
// Returns the index of the best blob - not sure if we need this
int trackBlob(CBlobResult &blobs, Mat &depthInfo, Mat &dest)
{
    assert(!depthInfo.empty());
    static CBlobGetXCenter xCenter;
    static CBlobGetYCenter yCenter;


// do some erosion on the depth image
    const int kernelSize = 5;
    //NOTE: dilate to get rid of image noise, make blobs bigger/join
    //
    Mat kernelMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*kernelSize+1, 2*kernelSize+1));
//
    cv::dilate(depthInfo, depthInfo, kernelMat);

blobs.Filter(blobs, B_EXCLUDE, CBlobGetArea(), B_GREATER, maxBlobArea);
blobs.Filter(blobs, B_EXCLUDE, CBlobGetArea(), B_LESS, minBlobArea);

// TODO: is there a faster way to get the closest to last known?
    int bestBlobIndex=0;
    double bestDistance = INFINITY;
    for (int i=0; i< blobs.GetNumBlobs(); i++) {
        CBlob b = blobs.GetBlob(i);
        double d = hypot(xCenter(b)-lastKnown.x, yCenter(b)-lastKnown.y);
        if (d < bestDistance) {
            bestBlobIndex = i;
            bestDistance = d;
        }
    }

    for (int i=0; i< blobs.GetNumBlobs(); i++) {
        CBlob t2 = blobs.GetBlob(i);
        if (i == bestBlobIndex) {
            //t2.FillBlob(dest, Scalar(255,92,0));
            lastKnown.x = xCenter(t2);
            lastKnown.y = yCenter(t2);
            cv::circle(dest, lastKnown, 25, Scalar(255,92,0), 3);
            // put the coords of the tracked blob on screen
            signed short clickDepth = depthInfo.at<signed short>(lastKnown.x, lastKnown.y);
            double worldX, worldY, worldZ;
            my_camera_to_world(lastKnown.x, lastKnown.y, clickDepth, &worldX, &worldY, &worldZ); 
            // log the values to disk
            Scalar textColor(120,120,120);

            if (doTrack){
                currentTime = clock();
                //cout << currentTime << "\t" << worldX << "\t" << worldY << "\t" << worldZ << "\n";
                logFile << currentTime << "\t" << lastKnown.x << "\t" << lastKnown.y << "\t" << clickDepth << "\n";
                textColor = Scalar(96,255,96);
            }

            if (worldZ != 0) {
                // don't display bad values
                const double fontScale = 1.2;
                ostringstream xStr;
                xStr << lastKnown.x;
                
                cv::putText(dest, xStr.str(), Point(0, 25), CV_FONT_HERSHEY_DUPLEX, fontScale, textColor);
                ostringstream yStr;
                yStr << lastKnown.y;
                
                cv::putText(dest, yStr.str(), Point(0, 75), CV_FONT_HERSHEY_DUPLEX, fontScale, textColor);
                ostringstream zStr;
                zStr << clickDepth;
                
                cv::putText(dest, zStr.str(), Point(0, 125), CV_FONT_HERSHEY_DUPLEX, fontScale, textColor);
            }
        } else {
            t2.FillBlob(dest, Scalar(92,92,192));
        }
    }
    return bestBlobIndex;
}

Mat prepareHSVImage(Mat &srcMat)
{
    const int dilationSize = 3;
    const int erosionSize = 3;
    //NOTE: dilate to get rid of image noise, make blobs bigger/join
    //
    Mat dilationMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*dilationSize+1, 2*dilationSize+1));
    Mat erosionMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*erosionSize+1, 2*erosionSize+1));
//
    Mat hsvMat(Size(imgWidth, imgHeight), CV_8UC1, Scalar(0));
    cvtColor(srcMat, hsvMat, CV_BGR2HSV); 
    inRange(hsvMat,  Scalar(0,0,0), Scalar(255,255,192), hsvMat);
    GaussianBlur(hsvMat, hsvMat, Size(3,3), 0);
    threshold(hsvMat, hsvMat, 90, 255, CV_THRESH_BINARY_INV);
             
    cv::erode(hsvMat, hsvMat, erosionMat);
    return hsvMat;
}

// obtained by experimenting with DEBUG set to 1
static int lowS=84;
static int highS=255;
static int lowV=12;
static int highV=240;

Mat prepareBlueRedImage(Mat &srcMat)
{
    const int dilationSize = 3;
    const int erosionSize = 3;
    //NOTE: dilate to get rid of image noise, make blobs bigger/join
    //
    Mat dilationMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*dilationSize+1, 2*dilationSize+1));
    Mat erosionMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*erosionSize+1, 2*erosionSize+1));

    Mat redMat(Size(imgWidth, imgHeight), CV_8UC1, Scalar(0));
    Mat blueMat(Size(imgWidth, imgHeight), CV_8UC1, Scalar(0));
    Mat imgMat(Size(imgWidth, imgHeight), CV_8UC1, Scalar(0));

    GaussianBlur(imgMat, imgMat, Size(3,3), 0);
    cvtColor(srcMat, imgMat, CV_BGR2HSV); 
    // BRIGHT BLUE from front LEDs
    inRange(imgMat, Scalar(100,lowS,lowV), Scalar(130,highS, highV), blueMat);

    // BRIGHT RED from back LEDS
    // red is on two ends of hsv spectrum, let's get the H and SV thresholds separately
    Mat tempMat(redMat);
    // SV
    inRange(imgMat, Scalar(0, lowS, lowV), Scalar(255, highS, highV), tempMat);

    // H
    inRange(imgMat, Scalar(15,0,0), Scalar(165, 255,255), redMat);
    bitwise_not(redMat, redMat);
    bitwise_and(tempMat, redMat, redMat);
    
    imgMat = redMat;

    cv::erode(imgMat, imgMat, erosionMat);
//    cv::dilate(imgMat, imgMat, erosionMat);
    return imgMat;
}


Mat prepareGrayImage(Mat &srcMat)
{
    const int dilationSize = 3;
    const int erosionSize = 3;
    //NOTE: dilate to get rid of image noise, make blobs bigger/join
    //
    Mat dilationMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*dilationSize+1, 2*dilationSize+1));
    Mat erosionMat = cv::getStructuringElement(MORPH_ELLIPSE, 
           Size(2*erosionSize+1, 2*erosionSize+1));
// just grayscale and threshold
    Mat grayMat(Size(imgWidth, imgHeight), CV_8UC1, Scalar(0));
    cvtColor(srcMat, grayMat, CV_BGR2HSV); 
    inRange(grayMat,  Scalar(25,lowS,lowV), Scalar(50,highS,highV), grayMat);
             
    //cv::dilate(grayMat, grayMat, dilationMat);
    cv::erode(grayMat, grayMat, erosionMat);
    return grayMat;
}

void trackbarCallback(int val, void *data)
{
    char *str = (char *)data;
    cout << str << ": " << val << "\n";
}

// give us time to capture weak matches
#define TRACK_TICKS 25
int main(int argc, char **argv) {
	bool die(false);
	string filename("kinect_tracking_");
	string suffix(".tsv");
	int i_snap(0),iter(0);

    int trackCounter = 0;
    ostringstream fullName;

    currentTime = clock();
    fullName << filename << currentTime<<suffix;

    lastKnown.x = imgWidth/2;
    lastKnown.y = imgHeight/2;

	Mat depthMat(Size(imgWidth,imgHeight),CV_16UC1);
	Mat depthf (Size(imgWidth,imgHeight),CV_8UC1);
	Mat rgbMat(Size(imgWidth,imgHeight),CV_8UC3,Scalar(0));
	
	Freenect::Freenect freenect;
	MyFreenectDevice& device = freenect.createDevice<MyFreenectDevice>(0);

    device.setDepthFormat(FREENECT_DEPTH_MM);
	
	namedWindow("depth",CV_WINDOW_AUTOSIZE);
	namedWindow("rgb",CV_WINDOW_AUTOSIZE);
#if DEBUG
	namedWindow("debug",CV_WINDOW_AUTOSIZE);
    createTrackbar("Min Saturation", "debug", &lowS, 255, trackbarCallback, (void*)"S");
    createTrackbar("Max Saturation", "debug", &highS, 255, trackbarCallback, (void*)"S"); 
    createTrackbar("Min Value", "debug", &lowV, 255, trackbarCallback, (void*)"V");
    createTrackbar("Max Value", "debug", &highV, 255, trackbarCallback, (void*)"V");
#endif
    setMouseCallback("rgb", onMouse,0);

	device.startVideo();
	device.startDepth();
    logFile.open(fullName.str());
	while (!die) {
		device.getVideo(rgbMat);
		device.getDepth(depthMat);

        Mat displayMat = rgbMat.clone();
        //Mat detectionMat = prepareBlueRedImage(displayMat);
        Mat detectionMat = prepareGrayImage(displayMat);

        CBlobResult res(detectionMat, Mat(), 2);
        /// show blobs on RGB image

        trackBlob(res, depthMat, displayMat);

#if DEBUG 
		cv::imshow("debug", detectionMat);
#endif
		cv::imshow("rgb", displayMat);

		depthMat.convertTo(depthf, CV_8UC1, 255.0/10000.0);
        // circle the object in the depth field too
        cv::circle(depthf, lastKnown, 10, CV_RGB(255,255,255), 2);
#if DEBUG
#endif
		cv::imshow("depth",depthf);

		char k = cvWaitKey(5);
		if( k == 27 ){
			cvDestroyWindow("rgb");
			cvDestroyWindow("depth");
			break;
		}

        if (doTrack) {
            trackCounter--;
            if (trackCounter <= 0) {
                trackCounter = 0;
                //doTrack = false;
                cout << "Done tracking\n";
                logFile.flush();
            }
        }

        if (!doTrack && k == '\t') {
            string userInfo;
            cout << "Coordinate: ";
            cin >> userInfo;
            logFile << "\n" << userInfo << "\n";

            doTrack = true;
            trackCounter = TRACK_TICKS;
            cout << "Tracking in file " << fullName.str() << "\n";
        }
	}
	
    logFile.close();
	device.stopVideo();
	device.stopDepth();
	return 0;
}
