all: tracker

CFLAGS = -fPIC -g -Wall 

CV_CFLAGS = `pkg-config --cflags opencv`
CV_LIBS = `pkg-config --libs opencv`

BLOB_CFLAGS = -IcvBlobsLib/library
BLOB_LIBS = -LcvBlobsLib/ -lopencvblobslib

KNECT_CFLAGS =  -I/usr/local/include/libfreenect 
KNECT_LIBS = -L/usr/local/lib -lfreenect 

USB_CFLAGS =-I/usr/local/include/libusb-1.0
USB_LIBS = -lusb-1.0

tracker:  MyFreenectDevice.o main.o
	$(CXX) $(CFLAGS) $^ -o $@  $(CV_LIBS) $(BLOB_LIBS) $(KNECT_LIBS) $(USB_LIBS)

main.o:	main.cpp MyFreenectDevice.h
	$(CXX) -c $(CV_CFLAGS) $(BLOB_CFLAGS) $(KNECT_CFLAGS) $(USB_CFLAGS) $(CFLAGS) $< -o $@

MyFreenectDevice.o:	MyFreenectDevice.cpp MyFreenectDevice.h
	$(CXX) -c $(CV_CFLAGS) $(KNECT_CFLAGS) $(USB_CFLAGS) $(CFLAGS) $< -o $@

clean:
	rm -rf *.o tracker
