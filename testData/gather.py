import numpy as np
from collections import defaultdict

#Kinect at 2.5,10 (x=lr, y=ud, z=fb)

def extractBlobs(textLine):
    # expects a line like
    # kinect.blobs: /kinect/blobs 1 0.39 0.75 ...

    values = textLine.split(':')[1].split()[1:]
    rawBlobs = zip(*[iter(values)]*6)

    blobs = [{'screenX':float(b[1]), 'screenY':float(b[2]), 
              'x': float(b[3]), 'y': float(b[4]), 'z':float(b[5])}
             for b in rawBlobs]

    return blobs


def calcAvgPosition(blobValues):
    # assumes blob 0 is always the blob we want
    xs, ys, zs = zip(*[(b[0]['x'], b[0]['y'], b[0]['z']) 
                        for b in blobValues])
    return np.mean(np.array(xs)), np.mean(np.array(ys)), np.mean(np.array(zs))

def extractTrajectory(blobValues, blobIdx=0):
    # take the world x,y,z of the blob we want
    # it is a big problem if there aren't enough blobs
    points = [np.array([b[blobIdx]['x'], b[blobIdx]['y'], b[blobIdx]['z']])
               for b in blobValues]
    return np.array(points)

def loadBlobData(filename):
    blobData = defaultdict(list)
    lastHeading=''
    wasHeading = False
    with open(filename, 'r') as dataFile:
        for line in dataFile:
            line = line.strip()
            if line.startswith('kinect.blobs'):
                blobs = extractBlobs(line)
                blobData[lastHeading].append(blobs)
                wasHeading = False
            elif len(line):
                if wasHeading:
                    lastHeading = lastHeading +" "+ line
                else:
                    lastHeading = line
                wasHeading = True
    return blobData

if __name__ == '__main__':
    blobData = loadBlobData('balloon.txt')
    for k in sorted(blobData.keys()):
        print k, "\t",calcAvgPosition(blobData[k])
    blobData = loadBlobData('experiment-kinect.txt')
    for k in sorted(blobData.keys()):
        print k, "\t",calcAvgPosition(blobData[k])
